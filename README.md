Metro for Steam
===============

http://metroforsteam.com

Description
-----------
A skin for Steam inspired by Microsoft's design language, Metro UI.

Installation
------------
1. Install the included font if necessary.
2. Copy "Metro for Steam" to "Steam/skins/". *(MAKE SURE ANOTHER METRO FOR STEAM FOLDER IS NOT INSIDE)*
3. In Steam, go to "Menu - Settings - Interface" and change "default skin" to "Metro for Steam" under the third dropdown menu.
4. Restart Steam

Customization
-------------
Accent color and fonts can be changed [online](http://metroforsteam.com/settings) or with the dedicated settings app, courtesy of SoapyHamHocks. Place it in your Metro for Steam folder.

It can be downloaded from the following links. Place it in your Metro for Steam folder before running.

[Executable](https://github.com/SoapyHamHocks/MetroSteamSettings/releases/download/v2.0.0/bin.zip)  
[Source](https://github.com/SoapyHamHocks/MetroSteamSettings)  

Credits
-------
**BoneyardBrew**  
**SoapyHamHocks** (Settings App, Feedback/Testing Machine)  
**Shawn** (Feedback and Build Testing)  
**Asphira** and **Inhibitor** (Feedback and Consulting)  
**Auroka** (Vector Steam Logo)  
**Everyone** (too many to list individually) who uses the theme and provides me with invaluable feedback! Thanks!  
